import {Map} from "immutable";

let Constants = Map({
    ADD_USER: 'addUser',
    UPDATE_CHAT: 'updateChat',
    UPDATE_ROOMS: 'updateRooms',
    SWITCH_ROOM: 'switchRoom',
    SEND_CHAT: 'sendChat',
});


export {Constants};
