import http from "http";
import SocketIO from "socket.io";
import {Chat} from "./models/chat";
import {Config} from "./config";
import {isEmpty} from "ramda";

class ChatServer {

    static createServerAndStartListening(serverPort) {
        if (isEmpty(serverPort)) {
            throw new Error("serverPort should not be empty");
        }
        //Create server
        const app = http.createServer();
        const io = new SocketIO(app);

        //Listen to the port
        app.listen(serverPort);

        //start the chat application
        const chat = new Chat();

        chat.start(io);
    }
}

const serverPort = Config.get('APP_PORT');
ChatServer.createServerAndStartListening(serverPort);
