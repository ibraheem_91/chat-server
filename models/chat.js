import {Constants} from "../helpers/constants";
import {List} from "immutable";
import {isEmpty, path} from "ramda";

const rooms = List(['room1', 'room2', 'room3']);

/**
 * Chat model
 */
class Chat {

    /**
     * start chat
     * @param io
     */
    start(io) {
        if (isEmpty(io)) {
            throw new Error('connection is not available');
        }
        //event listeners
        this.onAddUserToChat(io);
        this.onSendChatMessage(io);
        this.onSwitchRoom(io);
    };

    /**
     * Add user listener
     * @param io
     */
    onAddUserToChat(io) {

        io.on('connection', function (socket) {

            // when the client emits 'adduser', this listens and executes
            socket.on(Constants.get('ADD_USER'), function (data) {

                let currentUsername = '';
                if (!isEmpty(data)) {
                    currentUsername = path(['username'], data) || '';
                }
                // send client to room 1
                socket.join('room1');
                // echo to client they've connected
                socket.emit(Constants.get('UPDATE_CHAT'), currentUsername, 'has connected to room1', 'room1');

                // echo to room 1 that a person has connected to their room
                socket.broadcast.to('room1').emit(Constants.get('UPDATE_CHAT'), currentUsername, 'has connected to this room', 'room1');

                socket.emit(Constants.get('UPDATE_ROOMS'), rooms.toArray(), 'room1');
            });

        });

    };

    /**
     * SendChatMessage Listener
     * @param io
     */
    onSendChatMessage(io) {
        // when the client emits 'sendchat', this listens and executes
        io.on('connection', function (socket) {
            socket.on(Constants.get('SEND_CHAT'), function (data) {
                if (isEmpty(data)) {
                    return;
                }
                const msg = path(['msg'], data) || '';
                const username = path(['username'], data) || '';
                const room = path(['room'], data) || '';

                io.sockets.in(room).emit(Constants.get('UPDATE_CHAT'), username, msg, room);

            });
        });
    };

    /**
     * Switch Room Listener
     * @param io
     */
    onSwitchRoom(io) {

        io.on('connection', function (socket) {
            socket.on(Constants.get('SWITCH_ROOM'), function (data) {

                if (isEmpty(data)) {
                    return;
                }

                const oldRoom = path(['oldRoom'], data) || '';
                const newRoom = path(['newRoom'], data) || '';
                const username = path(['username'], data) || '';

                // leave the current room
                socket.leave(oldRoom);
                // join new room
                socket.join(newRoom);

                socket.emit(Constants.get('UPDATE_CHAT'), username, 'has connected to ' + newRoom, newRoom);

                socket.broadcast.to(newRoom).emit(Constants.get('UPDATE_CHAT'), username, 'has joined this room', newRoom);

                socket.emit(Constants.get('UPDATE_ROOMS'), rooms.toArray(), newRoom);

            });
        });

    }

}

export {Chat};