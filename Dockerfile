FROM alpine:3.4

# Update & install required packages
RUN apk add --update nodejs bash git

RUN apk --no-cache add --virtual native-deps \
  g++ gcc libgcc libstdc++ linux-headers make python

# Install app dependencies
COPY package.json /www/package.json
RUN cd /www; npm install --quiet

# Copy app source
COPY . /www

# Set work directory to /www
WORKDIR /www
