'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Constants = undefined;

var _immutable = require('immutable');

var Constants = (0, _immutable.Map)({
    ADD_USER: 'addUser',
    UPDATE_CHAT: 'updateChat',
    UPDATE_ROOMS: 'updateRooms',
    SWITCH_ROOM: 'switchRoom',
    SEND_CHAT: 'sendChat'
});

exports.Constants = Constants;
//# sourceMappingURL=constants.js.map