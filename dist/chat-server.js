"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _http = require("http");

var _http2 = _interopRequireDefault(_http);

var _socket = require("socket.io");

var _socket2 = _interopRequireDefault(_socket);

var _chat = require("./models/chat");

var _config = require("./config");

var _ramda = require("ramda");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ChatServer = function () {
    function ChatServer() {
        _classCallCheck(this, ChatServer);
    }

    _createClass(ChatServer, null, [{
        key: "createServerAndStartListening",
        value: function createServerAndStartListening(serverPort) {
            if ((0, _ramda.isEmpty)(serverPort)) {
                throw new Error("serverPort should not be empty");
            }
            //Create server
            var app = _http2.default.createServer();
            var io = new _socket2.default(app);

            //Listen to the port
            app.listen(serverPort);

            //start the chat application
            var chat = new _chat.Chat();

            chat.start(io);
        }
    }]);

    return ChatServer;
}();

var serverPort = _config.Config.get('APP_PORT');
ChatServer.createServerAndStartListening(serverPort);
//# sourceMappingURL=chat-server.js.map