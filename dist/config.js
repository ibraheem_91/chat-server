"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Config = undefined;

var _immutable = require("immutable");

var Config = (0, _immutable.Map)({
    APP_PORT: 3456
});

exports.Config = Config;
//# sourceMappingURL=config.js.map