"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Chat = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _constants = require("../helpers/constants");

var _immutable = require("immutable");

var _ramda = require("ramda");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var rooms = (0, _immutable.List)(['room1', 'room2', 'room3']);

/**
 * Chat model
 */

var Chat = function () {
    function Chat() {
        _classCallCheck(this, Chat);
    }

    _createClass(Chat, [{
        key: "start",


        /**
         * start chat
         * @param io
         */
        value: function start(io) {
            if ((0, _ramda.isEmpty)(io)) {
                throw new Error('connection is not available');
            }
            //event listeners
            this.onAddUserToChat(io);
            this.onSendChatMessage(io);
            this.onSwitchRoom(io);
        }
    }, {
        key: "onAddUserToChat",


        /**
         * Add user listener
         * @param io
         */
        value: function onAddUserToChat(io) {

            io.on('connection', function (socket) {

                // when the client emits 'adduser', this listens and executes
                socket.on(_constants.Constants.get('ADD_USER'), function (data) {

                    var currentUsername = '';
                    if (!(0, _ramda.isEmpty)(data)) {
                        currentUsername = (0, _ramda.path)(['username'], data) || '';
                    }
                    // send client to room 1
                    socket.join('room1');
                    // echo to client they've connected
                    socket.emit(_constants.Constants.get('UPDATE_CHAT'), currentUsername, 'has connected to room1', 'room1');

                    // echo to room 1 that a person has connected to their room
                    socket.broadcast.to('room1').emit(_constants.Constants.get('UPDATE_CHAT'), currentUsername, 'has connected to this room', 'room1');

                    socket.emit(_constants.Constants.get('UPDATE_ROOMS'), rooms.toArray(), 'room1');
                });
            });
        }
    }, {
        key: "onSendChatMessage",


        /**
         * SendChatMessage Listener
         * @param io
         */
        value: function onSendChatMessage(io) {
            // when the client emits 'sendchat', this listens and executes
            io.on('connection', function (socket) {
                socket.on(_constants.Constants.get('SEND_CHAT'), function (data) {
                    if ((0, _ramda.isEmpty)(data)) {
                        return;
                    }
                    var msg = (0, _ramda.path)(['msg'], data) || '';
                    var username = (0, _ramda.path)(['username'], data) || '';
                    var room = (0, _ramda.path)(['room'], data) || '';

                    io.sockets.in(room).emit(_constants.Constants.get('UPDATE_CHAT'), username, msg, room);
                });
            });
        }
    }, {
        key: "onSwitchRoom",


        /**
         * Switch Room Listener
         * @param io
         */
        value: function onSwitchRoom(io) {

            io.on('connection', function (socket) {
                socket.on(_constants.Constants.get('SWITCH_ROOM'), function (data) {

                    if ((0, _ramda.isEmpty)(data)) {
                        return;
                    }

                    var oldRoom = (0, _ramda.path)(['oldRoom'], data) || '';
                    var newRoom = (0, _ramda.path)(['newRoom'], data) || '';
                    var username = (0, _ramda.path)(['username'], data) || '';

                    // leave the current room
                    socket.leave(oldRoom);
                    // join new room
                    socket.join(newRoom);

                    socket.emit(_constants.Constants.get('UPDATE_CHAT'), username, 'has connected to ' + newRoom, newRoom);

                    socket.broadcast.to(newRoom).emit(_constants.Constants.get('UPDATE_CHAT'), username, 'has joined this room', newRoom);

                    socket.emit(_constants.Constants.get('UPDATE_ROOMS'), rooms.toArray(), newRoom);
                });
            });
        }
    }]);

    return Chat;
}();

exports.Chat = Chat;
//# sourceMappingURL=chat.js.map